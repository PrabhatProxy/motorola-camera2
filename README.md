# Motorola Moto Camera: List 0

<center><img src="https://telegra.ph/file/cf43da1a6481e7c094ff6.jpg"/></center>

Compatible Devices:

- Moto G5s Plus (sanders)
- Moto G6 (ali)
- Moto Green Pomelo 1s (ali)


**How to add it in your tree**

To clone:

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_camera2_primary-2.git -b eleven packages/apps/MotCamera2`

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_camera2_overlay.git -b sanders packages/apps/MotCamera2-overlay`

`git clone https://gitlab.com/NemesisDevelopers/motorola/motorola_motosignatureapp.git -b eleven packages/apps/MotoSignatureApp`

Add this in your dependencies:

```
 {
   "repository": "motorola_camera2_primary-2",
   "target_path": "packages/apps/MotCamera2",
   "branch": "eleven",
   "remote": "moto-camera"
 }
```
Add this in your device.mk or common.mk:

```
# Moto Camera 2
PRODUCT_PACKAGES += \
    MotCamera2
```

# [Download & info](https://telegra.ph/Moto-Camera-2-List-N0-05-30)


 Copyright © 2020-2021 Nemesis Team
